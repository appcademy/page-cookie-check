=== Check Cookie or Redirect ===
Contributors: Matteo Mazzoni
License: GPL3

This plugin gets and sets a cookie to check whether the user already visit the page/post.
If cookie is present, current page is redirected elsewhere