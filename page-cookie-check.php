<?php
/*
Plugin Name: Page Cookie Check
Version:     0.4.0
Author:      Matteo Mazzoni
Description: This plugin gets and sets a cookie to check whether the user already visit the page/post. If cookie is present, current page is redirected elsewhere
License:     GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// CONFIG AUTOUPDATE MECHANISM
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = PucFactory::buildUpdateChecker(
    'https://bitbucket.org/appcademy/page-cookie-check/raw/master/metadata.json',
    __FILE__
);

/** IDEA: il plugin è uno shortcode 
 ** Cosa accade quando viene fatto il rendering:
 ** 1. si legge l'esistenza di un cookie
 ** 1.a Se il cookie esiste, redirect verso URL specifico
 ** 1.b Se il cookie non esiste, setta il cookie e vai avanti
 **
 ** HOWTO: UNSET COOKIE
 ** # unset( $_COOKIE[$v_username] );
 ** # setcookie( $v_username, '', time() - ( 15 * 60 ) );
 **
 **/
if(!function_exists('_log')){
  function _log( $message ) {
    if( WP_DEBUG === true ){
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
    }
  }
}
function redirect_on_cookie( $atts ){
	// get page ID
	$the_id = get_the_ID();
    $salt = 'e605bc6a-c709-452a-9bf2-4d1e5653a4e7';
    // apply defaults to attributes
	$a = shortcode_atts( array(
    	'redirect' => home_url(),
    	'id' => $the_id,
		), $atts );

	$cookie_id=hash('sha512', $a['id'].$salt);
	
	if (isset($_COOKIE[$cookie_id])){
				$dest=$a['redirect'];
                _log("REDIRECTING TO $dest"); 
?>
<script type="text/javascript">
	window.location = "<?php echo $a['redirect']; ?>";
</script>
<?php
	//	wp_redirect( $a['redirect'], 302 );
		exit;
	} else {;;
                _log("SETTING COOKIE"); ?>
<script type="text/javascript">
	document.cookie = "<?php echo "$cookie_id=set"; ?>; expires=Thu, 18 Dec 2023 12:00:00 UTC";
</script><?php
		// setcookie( $cookie_id, $cookie_id, 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );	
	}
}
add_shortcode('redirect_on_cookie', 'redirect_on_cookie' );
add_shortcode('redirect-on-cookie', 'redirect_on_cookie' );

